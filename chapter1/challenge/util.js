const chalk = require("chalk");

const readline = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout,
});

const penjumlahan = (a, b) => `${a} + ${b} = ${a + b}`;
const pengurangan = (a, b) => `${a} - ${b} = ${a - b}`;
const perkalian = (a, b) => `${a} * ${b} = ${a * b}`;
const pembagian = (a, b) => `${a} / ${b} = ${a / b}`;
const kuadrat = (a, b) => `${a} ^ ${b} = ${a ** b}`;
const luasPersegi = (p, l) => `Luas Persegi: ${p * l}`;
const volumeKubus = (s) => `Volume Kubus: ${s ** 3}`;
const volumeTabung = (r, t) => `Volume Tabung: ${Math.PI * r ** 2 * t}`;

const cetakHasil = (hasil) => console.log(chalk.green.inverse.bold(hasil));

const perhitunganAngka = async () => {
  const a = await pertanyaan("Masukan angka pertama = ");
  const b = await pertanyaan("Masukan angka kedua = ");
  cetakPerhitungan(a, b);
};

const perhitunganLuasPersegi = async () => {
  const panjang = await pertanyaan("Masukkan panjang: ");
  const lebar = await pertanyaan("Masukkan lebar: ");
  const luas = luasPersegi(panjang, lebar);
  cetakHasil(luas);
};

const perhitunganVolumeKubus = async () => {
  const sisi = await pertanyaan("Masukkan sisi: ");
  const volume = volumeKubus(sisi);
  cetakHasil(volume);
};

const perhitunganVolumeTabung = async () => {
  const radius = await pertanyaan("Masukkan radius: ");
  const tinggi = await pertanyaan("Masukkan tinggi: ");
  const volume = volumeTabung(radius, tinggi);
  cetakHasil(volume);
};

const noResult = () => {
  console.log(chalk.red.inverse.bold("--- Pilihan tidak ada ---"));
  return false;
};

const cetakPerhitungan = (a, b) => {
  cetakHasil("Hasil Penjumlahan " + penjumlahan(a, b));
  cetakHasil("Hasil Pengurangan " + pengurangan(a, b));
  cetakHasil("Hasil Perkalian " + perkalian(a, b));
  cetakHasil("Hasil Pembagian " + pembagian(a, b));
  cetakHasil("Hasil Kuadrat " + kuadrat(a, b));
};

const pertanyaan = (question) => {
  return new Promise((resolve, reject) => {
    readline.question(question, (name) => {
      resolve(name);
    });
  });
};

const close = () => readline.close();

module.exports = {
  perhitunganAngka,
  perhitunganLuasPersegi,
  perhitunganVolumeKubus,
  perhitunganVolumeTabung,
  noResult,
  pertanyaan,
  close,
};
