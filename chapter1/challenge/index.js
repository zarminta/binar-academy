/*  npm install first then run node index or npm start on terminal
    Soal challege : Buatlah sebuah program kalkulasi tambah kurang kali bagi akar quadrat, luas persegi,volume kubus, volume tabung dengan input dari terminal
*/

const {
  perhitunganAngka,
  perhitunganLuasPersegi,
  perhitunganVolumeKubus,
  perhitunganVolumeTabung,
  noResult,
  pertanyaan,
  close,
} = require("./util");

const main = async () => {
  const pilihan = await pertanyaan(
    "Pilih perhitungan yang ingin dilakukan : \n1. Angka \n2. Luas Persegi \n3. Volume Kubus \n4. Volume Tabung \n Pilih : "
  );
  switch (pilihan) {
    case "1":
      await perhitunganAngka();
      break;
    case "2":
      await perhitunganLuasPersegi();
      break;
    case "3":
      await perhitunganVolumeKubus();
      break;
    case "4":
      await perhitunganVolumeTabung();
      break;
    default:
      noResult();
      break;
  }

  close();
};

main();
